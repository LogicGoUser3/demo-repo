package com.example.retrofitdemo.model;

import com.google.gson.annotations.SerializedName;

public class LoginModel {

    @SerializedName("success")
    boolean success;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    Professor_Data data;

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public Professor_Data getData() {
        return data;
    }

    public static class Professor_Data {
        @SerializedName("access_token")
        String access_token;

        @SerializedName("token_type")
        String token_type;

        @SerializedName("type")
        String type;

        @SerializedName("professor")
        ProfessorClass professor;

        public String getAccess_token() {
            return access_token;
        }

        public String getToken_type() {
            return token_type;
        }

        public String getType() {
            return type;
        }

        public ProfessorClass getProfessor() {
            return professor;
        }

        public static class ProfessorClass {
            @SerializedName("name")
            String name;

            @SerializedName("username")
            String username;

            public String getName() {
                return name;
            }

            public String getUsername() {
                return username;
            }
        }

    }


}
